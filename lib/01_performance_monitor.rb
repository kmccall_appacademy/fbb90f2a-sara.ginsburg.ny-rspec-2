require 'time'

def measure(num = 1, &prc)
  curr_time = Time.now
  num.times {prc.call}
  after_time = Time.now
  average_time = (after_time - curr_time) / num
end

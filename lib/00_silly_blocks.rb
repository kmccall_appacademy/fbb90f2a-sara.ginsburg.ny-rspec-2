def reverser(&prc)
  reversed = []
  sentence = prc.call
  sentence.split(" ").each {|word| reversed << word.reverse}
  reversed.join(" ")
end

def adder(n = 1, &prc)
  n + prc.call
end

def repeater(n = 1, &prc)
  for i in 1..n
    prc.call
  end
end
